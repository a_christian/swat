<?php

namespace Swat\Commands;

use Acquia\Blt\Robo\BltTasks;

class SetupCommand extends BltTasks {

  /**
   * Inits Bitbucket pipline
   *
   * @command setup
   * @description Main function to startup a project using swat.
   */
   public function swatSetup() {
     $this->invokeCommands([
       'shell-alias',
       'bitbucket:pipeline',
       'docker:create'
     ]);
   }

}
