<?php

namespace Swat\Commands;

use Acquia\Blt\Robo\BltTasks;
use Robo\Contract\VerbosityThresholdInterface;

/**
 * Defines commands for installing and updating the BLT alias.
 */
class AliasCommand extends BltTasks {

  /**
   * Installs the swat alias for command line usage.
   *
   * @command shell-alias
   */
  public function installSwatAlias() {
    $config_file = $this->getCliConfigFile();
    if (is_null($config_file)) {
      $this->logger->warning("Could not find your CLI configuration file.");
      $this->logger->warning("Looked in ~/.zsh, ~/.bash_profile, ~/.bashrc, ~/.profile, and ~/.functions.");
    }
    else {
      $this->say("SWAT can automatically create a Bash alias to make it easier to run SWAT tasks.");
      $this->say("This alias will be created in <comment>$config_file</comment>.");
      $confirm = $this->confirm("Install alias?");
      if ($confirm) {
        $this->createNewAlias();
      }
    }
  }

  /**
   * Creates a new SWAT alias in appropriate CLI config file.
   */
  protected function createNewAlias() {
    $this->say("Installing <comment>swat</comment> alias...");
    $config_file = $this->getCliConfigFile();
    if (is_null($config_file)) {
      $this->logger->error("Could not install blt alias. No profile found. Tried ~/.zshrc, ~/.bashrc, ~/.bash_profile, ~/.profile, and ~/.functions.");
    }
    else {
      $canonical_alias = file_get_contents($this->getConfigValue('repo.root') . '/vendor/imagex/swat/scripts/alias');

      /** @var \Robo\Result $result */
      $result = $this->taskWriteToFile($config_file)
        ->text($canonical_alias)
        ->append(TRUE)
        ->setVerbosityThreshold(VerbosityThresholdInterface::VERBOSITY_VERBOSE)
        ->run();

      if (!$result->wasSuccessful()) {
        throw new \Exception("Unable to install SWAT alias.");
      }

      $this->say("<info>Added alias for swat to $config_file.</info>");
      $this->say("You may now use the <comment>swat</comment> command from anywhere within a repository.");
      $this->say("Restart your terminal session or run <comment>source $config_file</comment> to use the new command.");
    }
  }

  /**
   * Determines the CLI config file.
   *
   * @return null|string
   *   Returns file path or NULL if none was found.
   */
  protected function getCliConfigFile() {
    $file = NULL;
    $user = posix_getpwuid(posix_getuid());
    $home_dir = $user['dir'];

    if (strstr(getenv('SHELL'), 'zsh')) {
      $file = $home_dir . '/.zshrc';
    }
    elseif (file_exists($home_dir . '/.bash_profile')) {
      $file = $home_dir . '/.bash_profile';
    }
    elseif (file_exists($home_dir . '/.bashrc')) {
      $file = $home_dir . '/.bashrc';
    }
    elseif (file_exists($home_dir . '/.profile')) {
      $file = $home_dir . '/.profile';
    }
    elseif (file_exists($home_dir . '/.functions')) {
      $file = $home_dir . '/.functions';
    }

    return $file;
  }

}
