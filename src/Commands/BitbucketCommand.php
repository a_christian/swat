<?php

namespace Swat\Commands;

use Acquia\Blt\Robo\BltTasks;

class BitbucketCommand extends BltTasks {

  /**
   * Inits Bitbucket pipline
   *
   * @command bitbucket:pipeline
   * @description Helper for bitbucket pipllines.
   */
   public function bitbucketSetup() {
     $this->invokeCommand('recipes:ci:bitbucket:init');
   }

}
