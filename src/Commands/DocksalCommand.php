<?php

namespace Swat\Commands;

use Acquia\Blt\Robo\BltTasks;
use Robo\Robo;
use Symfony\Component\Yaml\Yaml;

/**
 * Defines commands in the "custom" namespace.
 */
class DocksalCommand extends BltTasks {

  const DOCKSAL_VERSION = '2.1';

  /**
   * Create a new docksal env setup.
   *
   * @command docksal:create
   * @description Creates a docksal project.
   *
   * @option $db-name Defines the database name.
   * @option $db-user Defines the database user.
   * @option $db-pass Defines the database pw.
   */
  public function create($options = ['db-name' => '', 'db-user' => '', 'db-pass' => '']) {
    $repo_root = $this->getConfigValue('repo.root');
    // Ensure webroot exists.
    if (!is_dir($repo_root)) {
      throw new \Exception('Your Drupal webroot does not exist.');
    }

    $docksal_dir = $repo_root . '/.docksal';
    if (is_dir($docksal_dir)) {
      if (!$this->confirm(".docksal dir exists, are you sure you want to proceed?")) {
        return;
      }
    }

    $this->say("<info>Setting up docksal defaults...</info>");

    // Create .docksal dir with defaults.
    $create = $this->taskFilesystemStack()->mkdir($docksal_dir)->run();
    if (!$create->wasSuccessful()) {
      throw new \Exception("Unable to create .docksal directory.");
    }

    $docksal_defaults = $this->getConfigValue('swat.root') . '/defaults/docksal';
    $copy = $this->taskCopyDir([$docksal_defaults => $docksal_dir])
      ->run();
    if (!$copy->wasSuccessful()) {
      throw new \Exception("Unable to create .docksal directory.");
    }

    // Write simple config files.
    $docroot = ltrim(str_replace($repo_root, "", $this->getConfigValue('docroot')), '/');
    $docksal_env = $this->taskWriteToFile($docksal_dir . '/docksal.env')
      ->line('DOCKSAL_STACK=default')
      ->line('DOCROOT=' . $docroot)
      ->line('VIRTUAL_HOST=' . $this->getConfigValue('project.local.hostname'))
      ->line('');

    // Allow setting DB options.
    if (!empty($options['db-name']) || !empty($options['db-user']) || !empty($options['db-pass'])) {
      $docksal_env->line('# MYSQL');

      if (!empty($options['db-name'])) {
        $docksal_env->line('MYSQL_DATABASE=' . $options['db-name']);
      }

      if (!empty($options['db-user'])) {
        $docksal_env->line('MYSQL_USER=' . $options['db-user']);
      }

      if (!empty($options['db-pass'])) {
        $docksal_env->line('MYSQL_PASSWORD=' . $options['db-pass']);
      }
    }
    $docksal_env->run();

    // Local env.
    $this->taskWriteToFile($docksal_dir . '/docksal-local.env')
      ->line('# XDEBUG_ENABLED=1')
      ->run();

    // Docksal yml.
    $yaml = [];
    $this->defaultYml($yaml, $options);

    // Mail check.
    if ($this->confirm('Do you want to enable MailHog')) {
      $this->addMailHog($yaml, $options);
    }

    // PMA check.
    if ($this->confirm('Do you want to enable PHPMyAdmin')) {
      $this->addPhpMyAdmin($yaml, $options);
    }

    // Dump the file.
    $docksal_yml = Yaml::dump($yaml, 5, 2);
    $this->taskWriteToFile($docksal_dir . '/docksal.yml')
      ->text($docksal_yml)
      ->run();

    // Start the container and add the host.
    $this->taskExecStack()
      ->dir($repo_root)
      ->exec('fin up')
      ->exec('fin hosts add')
      ->run();

    $this->yell("Docksal is Setup! You should probably take a break, you've earned it.");
  }

  /**
   * Sets the default options for docksal.yml.
   *
   * @param array $yaml
   *   The docksal yaml array.
   * @param $options
   *   The args from the original command.
   */
  protected function defaultYml(&$yaml, $options) {
    $yaml['version'] = self::DOCKSAL_VERSION;

    if (!empty($options['db-name']) || !empty($options['db-user']) || !empty($options['db-pass'])) {
      $cli = [];
      $db = [];

      if (!empty($options['db-name'])) {
        $cli[] = 'MYSQL_DATABASE';
        $db[] = 'MYSQL_DATABASE=${MYSQL_DATABASE}';
      }

      if (!empty($options['db-user'])) {
        $cli[] = 'MYSQL_USER';
        $db[] = 'MYSQL_USER=${MYSQL_USER}';
      }

      if (!empty($options['db-pass'])) {
        $cli[] = 'MYSQL_PASSWORD';
        $db[] = 'MYSQL_PASSWORD=${MYSQL_PASSWORD}';
      }

      $yaml['services']['cli']['environment'] = $cli;
      $yaml['services']['db']['environment'] = $db;
    }
  }

  /**
   * Adds Mail to docksal.yml.
   *
   * @param array $yaml
   *   The docksal yaml array.
   * @param $options
   *   The args from the original command.
   */
  protected function addMailHog(&$yaml, $options) {
    $yaml['services']['mail']['extends'] = [
      'file' => '${HOME}/.docksal/stacks/services.yml',
      'service' => 'mail',
    ];
  }

  /**
   * Adds PMA to docksal.yml.
   *
   * @param array $yaml
   *   The docksal yaml array.
   * @param $options
   *   The args from the original command.
   */
  protected function addPhpMyAdmin(&$yaml, $options) {
    $yaml['services']['pma'] = [
      'hostname' => 'pma',
      'image' => 'phpmyadmin/phpmyadmin',
      'environment' => [
        'PMA_HOST=db',
        'PMA_USER=root',
        'PMA_PASSWORD=${MYSQL_ROOT_PASSWORD:-root}',
      ],
      'labels' => [
        'io.docksal.virtual-host=pma.${VIRTUAL_HOST}',
      ],
    ];
  }

}
