<?php

namespace Swat;

use Robo\Robo;
use Robo\Config\Config;
use Robo\Common\ConfigAwareTrait;
use Robo\Runner as RoboRunner;
use Acquia\Blt\Robo\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Consolidation\AnnotatedCommand\CommandFileDiscovery;

/**
 * Class Swat
 *
 * @package Swat
 */
class Swat {

  use ConfigAwareTrait;

  private $runner;

  const NAME = 'Swat';

  const VERSION = '1.0.0';

  /**
   * Swat constructor.
   *
   * @param Config $config
   * @param InputInterface|null $input
   * @param OutputInterface|null $output
   */
  public function __construct(Config $config, InputInterface $input = NULL, OutputInterface $output = NULL) {
    // Create application.
    $this->setConfig($config);

    $application = new Application(self::NAME, self::VERSION);

    $application->getDefinition()->addOptions([
      new InputOption(
        '--yes',
        '-y',
        InputOption::VALUE_NONE,
        'Automatically respond "yes" to all confirmation questions.'
      ),
    ]);

    // Create and configure container.
    $container = Robo::createDefaultContainer($input, $output, $application, $config);

    $discovery = new CommandFileDiscovery();
    $discovery->setSearchPattern('*Command.php');
    $commandClasses = $discovery->discover(__DIR__ . '/Commands', '\Swat\Commands');

    // Add in BLT commands.
    $discovery->setSearchLocations([]);
    $bltClasses = $discovery->discover($this->getConfig()->get('blt.root') . '/src/Robo/Commands', 'Acquia\Blt\Robo\Commands');

    // Instantiate Robo Runner.
    $this->runner = new RoboRunner([]);
    $this->runner->setContainer($container);
    $this->runner->registerCommandClasses($application, $bltClasses);
    $this->runner->registerCommandClasses($application, $commandClasses);
  }

  /**
   * @param InputInterface $input
   * @param OutputInterface $output
   *
   * @return int
   */
  public function run(InputInterface $input, OutputInterface $output) {
    $statusCode = $this->runner->run($input, $output);
    return $statusCode;
  }
}
