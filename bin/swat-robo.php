<?php

use Swat\Swat;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Acquia\Blt\Robo\Config\ConfigInitializer;
use Robo\Common\TimeKeeper;

if (file_exists(dirname(__DIR__) . '/vendor/autoload.php')) {
  $repo_root = dirname(__DIR__);
  require_once dirname(__DIR__) . '/vendor/autoload.php';
}
elseif (file_exists(dirname(__DIR__) . '/../../autoload.php')) {
  $repo_root = dirname(__DIR__) . '/../../..';
  require_once dirname(__DIR__) . '/../../autoload.php';
}

// Start Timer.
$timer = new TimeKeeper();
$timer->start();

// Initialize input and output.
$input = new ArgvInput($_SERVER['argv']);
$output = new ConsoleOutput();

// Write Swat version for debugging.
if ($output->isVerbose()) {
  $output->writeln("<comment>Swat version " . Swat::VERSION . "</comment>");
}

// Initialize configuration.
$config_initializer = new ConfigInitializer($repo_root, $input);
$config = $config_initializer->initialize();

// Swat root.
$config->set('swat.root', $repo_root . '/vendor/imagex/swat');

// Execute command.
$swat = new Swat($config, $input, $output);
$status_code = (int) $swat->run($input, $output);

// Stop timer.
$timer->stop();
if ($output->isVerbose()) {
  $output->writeln("<comment>" . $timer->formatDuration($timer->elapsed()) . "</comment> total time elapsed.");
}

exit($status_code);
